const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()


router.get('/:id', (request, response) => {
	TaskController.getTask(request.params.id, request.body).then((task) => response.send(task))
})

router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task) => response.send(task))
})

router.put('/:id/complete', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask))
})


module.exports = router
